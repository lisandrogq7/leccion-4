var mymap = L.map('main_map').setView([3.4699565,-76.4907067], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

// CARGA DE MARCADORES
$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'natori6@maltacp.com', password: 'demo123' },
}).done(function( data ) {
    console.log(data);

    $.ajax({
        dataType: 'json',
        url: 'api/bicicletas',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function (result) {
        console.log(result);

        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    });
});