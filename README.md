# Red-Bicicletas | Lisandro Gonzalez

## Instalar Módulos de Node

Ejecutar `npm install` dentro de la carpeta del proyecto `red-bicicletas`, para descargar los paquetes o modulos de node requeridos para poder ejecutar la aplicación. 

## Iniciar Servidor de Desarrollo

Ejecutar `npm run devstart` para iniciar el servidor web. URL: `http://localhost:3000/`

## RealizarTests con Jasmine

Ejecutar `npm test` para ejecutar las pruebas de modelos de Bicicleta y Usuario y de API de Bicicleta de la carpeta spec.

## Ruta de Lista de Bicicletas (CRUD)

- `http://localhost:3000/bicicletas`

## Endpoints de la API Bicicletas

1. API GET - Listar Bicicletas   => `http://localhost:3000/api/bicicletas`
2. API POST - Crear Bicicleta    => `http://localhost:3000/api/bicicletas/create`
3. API PUT - Modificar Bicicleta => `http://localhost:3000/api/bicicletas/update`
4. API DELETE - Borrar Bicicleta => `http://localhost:3000/api/bicicletas/delete`

## Endpoints de la API Usuarios

1. API GET - Listar Usuarios     => `http://localhost:3000/api/usuarios`
2. API POST - Crear Usuario      => `http://localhost:3000/api/usuarios/create`
3. API POST - Crear Reserva      => `http://localhost:3000/api/usuarios/reservar`

## Endpoints de la API Autenticacion

1. API POST Auth                => `http://localhost:3000/api/auth/authenticate`
2. API POST Auth Facebook Token => `http://localhost:3000/api/auth/facebook_token`


## Configuracion de Variables de Entorno

Para el correcto funcionamiento de el sistema, se debe configurar todas las variables de entorno. Caso contrario, el sistema no funcionara correctamente.

